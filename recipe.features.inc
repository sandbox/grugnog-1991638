<?php
/**
 * @file
 * recipe.features.inc
 */

/**
 * Implements hook_node_info().
 */
function recipe_node_info() {
  $items = array(
    'recipe' => array(
      'name' => t('Recipe'),
      'base' => 'node_content',
      'description' => t('A set of ingredients and instructions that describe how to prepare or make a dish.'),
      'has_title' => '1',
      'title_label' => t('Recipe name'),
      'help' => '',
    ),
  );
  return $items;
}
