<?php
/**
 * @file
 * recipe_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function recipe_search_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "current_search" && $api == "current_search") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function recipe_search_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function recipe_search_default_search_api_index() {
  $items = array();
  $items['recipe'] = entity_import('search_api_index', '{
    "name" : "Recipe",
    "machine_name" : "recipe",
    "description" : null,
    "server" : null,
    "item_type" : "node",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "title" : { "type" : "text" },
        "language" : { "type" : "string" },
        "created" : { "type" : "date" },
        "changed" : { "type" : "date" },
        "field_recipe_author" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_recipe_carbohydrates" : { "type" : "integer" },
        "field_recipe_cook_time" : { "type" : "integer" },
        "field_recipe_cuisine" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_recipe_dietary" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_recipe_directions" : { "type" : "list\\u003Ctext\\u003E" },
        "field_recipe_fiber" : { "type" : "integer" },
        "field_recipe_ingredients" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "field_collection_item"
        },
        "field_recipe_materials" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_recipe_meal_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_recipe_prep_time" : { "type" : "integer" },
        "field_recipe_protein" : { "type" : "integer" },
        "field_recipe_saturated_fat" : { "type" : "integer" },
        "field_recipe_sodium" : { "type" : "integer" },
        "field_recipe_source" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_recipe_storage_tips" : { "type" : "text" },
        "field_recipe_substitution_tips" : { "type" : "text" },
        "field_recipe_total_calories" : { "type" : "integer" },
        "field_recipe_total_fat" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "field_recipe_head_notes:value" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "recipe" : "recipe" } }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_recipe_directions" : true,
              "field_recipe_storage_tips" : true,
              "field_recipe_substitution_tips" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "field_recipe_directions" : true,
              "field_recipe_storage_tips" : true,
              "field_recipe_substitution_tips" : true,
              "field_recipe_head_notes:value" : true
            },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_recipe_directions" : true,
              "field_recipe_storage_tips" : true,
              "field_recipe_substitution_tips" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_recipe_directions" : true,
              "field_recipe_storage_tips" : true,
              "field_recipe_substitution_tips" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "0",
    "read_only" : "0"
  }');
  return $items;
}
