<?php
/**
 * @file
 * recipe.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function recipe_taxonomy_default_vocabularies() {
  return array(
    'recipe_authors' => array(
      'name' => 'Authors',
      'machine_name' => 'recipe_authors',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -10,
    ),
    'recipe_cuisine' => array(
      'name' => 'Cuisine',
      'machine_name' => 'recipe_cuisine',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -7,
    ),
    'recipe_dietary' => array(
      'name' => 'Dietary info',
      'machine_name' => 'recipe_dietary',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -6,
    ),
    'recipe_food' => array(
      'name' => 'Food',
      'machine_name' => 'recipe_food',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'recipe_materials' => array(
      'name' => 'Materials',
      'machine_name' => 'recipe_materials',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -5,
    ),
    'recipe_meal_type' => array(
      'name' => 'Meal type',
      'machine_name' => 'recipe_meal_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -8,
    ),
    'recipe_sources' => array(
      'name' => 'Sources',
      'machine_name' => 'recipe_sources',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -9,
    ),
  );
}
